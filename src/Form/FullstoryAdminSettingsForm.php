<?php

namespace Drupal\fullstory\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure FullStory Integration settings for this site.
 */
class FullstoryAdminSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a FullstoryAdminSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fullstory_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['fullstory.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fullstory.settings');

    // General Settings.
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
      '#group' => 'fullstory',
    ];
    $form['general']['fullstory_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $config->get('fullstory_host'),
      '#size' => 20,
      '#maxlength' => 20,
      '#disabled' => TRUE,
      '#description' => $this->t('For successful recording, this value should always be fullstory.com.'),
    ];
    $form['general']['fullstory_script'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Script'),
      '#default_value' => $config->get('fullstory_script'),
      '#size' => 20,
      '#maxlength' => 20,
      '#disabled' => TRUE,
    ];
    $form['general']['fullstory_org'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID'),
      '#default_value' => $config->get('fullstory_org'),
      '#size' => 20,
      '#maxlength' => 20,
      '#required' => TRUE,
      '#description' => $this->t('The ID of the org or account into which data should be recorded.'),
    ];
    $form['general']['fullstory_namespace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Namespace'),
      '#default_value' => $config->get('fullstory_namespace'),
      '#size' => 20,
      '#maxlength' => 20,
      '#required' => TRUE,
      '#description' => $this->t('Used to change the namespace for the FullStory API, to prevent namespace conflicts.'),
    ];

    $form['general']['fullstory_sampling'] = [
      '#type' => 'select',
      '#options' => [
        1 => $this->t('10%'),
        2 => $this->t('20%'),
        3 => $this->t('30%'),
        4 => $this->t('40%'),
        5 => $this->t('50%'),
        6 => $this->t('60%'),
        7 => $this->t('70%'),
        8 => $this->t('80%'),
        9 => $this->t('90%'),
        0 => $this->t('100%'),
      ],
      '#default_value' => $config->get('fullstory_sampling'),
      '#title' => $this->t('Percentage of eligible sessions to record'),
      '#description' => $this->t('Optionally limit the number of sessions recorded by 
      FullStory to a random sample of all your eligible sessions.'),
    ];

    $form['general']['fullstory_debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Turn on debug mode.'),
      '#default_value' => $config->get('fullstory_debug'),
      '#description' => $this->t('Check this if you want to see detailed console logs, and lots of them.'),
    ];

    // Role specific settings.
    $form['role_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Role specific script settings'),
      '#collapsible' => TRUE,
      '#group' => 'fullstory',
    ];

    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $role_names = array_map(function ($item) {
      return $item->label();
    }, $roles);
    $form['role_settings']['fullstory_exclude_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Remove script for specific roles'),
      '#default_value' => $config->get('fullstory_exclude_roles') ?? [],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', $role_names),
      '#description' => $this->t('Select any roles that should be excluded from FullStory recordings. If no roles are selected, all roles will be recorded.'),
    ];

    // Page specific settings.
    $form['page_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Page specific script settings'),
      '#collapsible' => TRUE,
      '#group' => 'fullstory',
    ];
    $form['page_settings']['fullstory_record_pages'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add script to specific pages'),
      '#options' => [
        $this->t('Record every page except the listed pages.'),
        $this->t('Record the listed pages only.'),
      ],
      '#default_value' => $config->get('fullstory_record_pages'),
    ];
    $form['page_settings']['fullstory_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#default_value' => $config->get('fullstory_pages'),
      '#description' => $this->t('Enter one page per line as Drupal paths. The "*" character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.', [
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>',
      ]),
      '#wysiwyg' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('fullstory.settings')
      ->set('fullstory_host', $form_state->getValue('fullstory_host'))
      ->set('fullstory_org', $form_state->getValue('fullstory_org'))
      ->set('fullstory_namespace', $form_state->getValue('fullstory_namespace'))
      ->set('fullstory_sampling', $form_state->getValue('fullstory_sampling'))
      ->set('fullstory_debug', $form_state->getValue('fullstory_debug'))
      ->set('fullstory_exclude_roles', $form_state->getValue('fullstory_exclude_roles'))
      ->set('fullstory_record_pages', $form_state->getValue('fullstory_record_pages'))
      ->set('fullstory_pages', $form_state->getValue('fullstory_pages'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
