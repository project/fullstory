
FullStory Integration
=====================

Introduction
------------

This FullStory integration module lets you quickly add the FullStory javascript
snippet, with your custom variables, to your Drupal site. This is especially
useful if you host a multisite setup; you can configure the variables for each
site separately to generate a custom snippet for each site.

https://www.fullstory.com/

* This integration is based on the help doc here:
  https://help.fullstory.com/hc/en-us/articles/360020624234-Roll-Your-Own-Integration

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/fullstory


Requirements
------------

You must have an account with FullStory. At the top of the snippet they provide,
look for this line: `window['_fs_org'] = 'ABC123';`

`ABC123` is your organization or account ID.


Installation
------------

* Install as you would normally install a contributed Drupal module. Visit:
  https://www.drupal.org/docs/extending-drupal/installing-modules
  for further information.


Configuration
-------------

* Configure user permissions in Administration » People » Permissions:

  - Administer FullStory module
    Permission to change FullStory settings.

* Customize the widget settings in Administration » Configuration » System »
  FullStory Integration.

  - Set the FullStory ID to the ID for your org or account.

  - Configure Role and Page specific settings to exclude some roles or pages
    from recording.

  - If you have many more monthly visitors than sessions available in your 
    FullStory subscription plan, you can set the module to only sample a random
    percentage of users (between 10%-90%).

Credits
-------
<!--- cspell:disable -->
* [Jenna Tollerson](https://drupal.org/u/jenna.tollerson) for the
  [State of Georgia](https://www.drupal.org/state-of-georgia)
* [Aaron Ferris](https://www.drupal.org/u/aaronferris)
<!--- cspell:enable -->
