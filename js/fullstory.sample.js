/**
 * @file
 * Add FullStory to page for a random sample of users.
 */

(function ($, drupalSettings, cookies) {
  if (cookies.get('run_fs_for_user') == null) {
    const fsSampleValue =
      Math.floor(Math.random() * 10) + 1 <= drupalSettings.fullstory.rate;
    cookies.set('run_fs_for_user', fsSampleValue, { path: '/' });
  }
  // Note here we're not looking for a boolean, we're matching a string value.
  if (cookies.get('run_fs_for_user') === 'true') {
    /* eslint-disable */
    /* The snippet below is provided by FullStory */
    window['_fs_debug'] = drupalSettings.fullstory.debug;
    window['_fs_host'] = drupalSettings.fullstory.host;
    window['_fs_script'] = drupalSettings.fullstory.script;
    window['_fs_org'] = drupalSettings.fullstory.org;
    window['_fs_namespace'] = drupalSettings.fullstory.namespace;
    (function(m,n,e,t,l,o,g,y){
      if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
      g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[];
      o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_script;
      y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
      g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)};
      g.anonymize=function(){g.identify(!!0)};
      g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
      g.log = function(a,b){g("log",[a,b])};
      g.consent=function(a){g("consent",!arguments.length||a)};
      g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
      g.clearUserCookie=function(){};
      g._w={};y='XMLHttpRequest';g._w[y]=m[y];y='fetch';g._w[y]=m[y];
      if(m[y])m[y]=function(){return g._w[y].apply(this,arguments)};
      g._v="1.2.0";
    })(window,document,window['_fs_namespace'],'script','user');
    /* eslint-enable */
  }
})(jQuery, drupalSettings, window.Cookies);
